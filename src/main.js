import { createApp } from 'vue'
import { createRouter,createWebHistory } from 'vue-router';
import TeamsList from './components/teams/TeamsList.vue';
import UsersList from './components/users/UsersList.vue';
import TeamMembers from './components/teams/TeamMembers.vue';
import NotFound from './components/nav/NotFound.vue';
import TeamsFooter from './components/teams/TeamsFooter.vue';
import UsersFooter from './components/users/UsersFooter.vue';

import App from './App.vue'
// eslint-disable-next-line
const router = createRouter({
    history: createWebHistory(),
    routes:[
        {path:'/',redirect:'/teams'},
        {
            name: 'teams',
            path: '/teams', //component: TeamsList, // alias:'/' autre possibilité
            components:{
                default: TeamsList,
                footer: TeamsFooter
            },
            children: [
                {name: 'team-members',path: ':teamId', component: TeamMembers, props:true},
            ]
    
        },
        {
            path: '/users', //component: UsersList,
            components:{
                default:UsersList,
                footer: UsersFooter
            },
            beforeEnter(to,from,next){
                console.log('users beforeEnter');
                console.log(to,from);
                next();
            }
    },
        // if you dont add 'props:true', the props are not accessibles in the page and its mandatory to named the props of the sane name as the param --> teamId
        
        {path:'/:notFound(.*)',component: NotFound}

    ],
    linkActiveClass: 'active',
    /*eslint-disable*/ 
    scrollBehavior(_,_2,savePosition){
        //console.log('to',to);
        //console.log('from',from);
        //console.log('savePosition',savePosition);
        return {
            left:0,
            top:0
        }
    }
});

router.beforeEach(function(to,from,next) {
    //console.log('Global for each');
    //console.log(to,from);
    // if(to.name ==='team-members')
    //     next();
    // else    
    //     next({name:'team-members',params:{teamId:'t3'}});
    next();

})
router.afterEach(function(to,from){
    console.log('Global afterEach');
    console.log(to,from);
})

const app = createApp(App);
app.use(router);
app.mount('#app')
